package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"text/tabwriter"

	"golang.org/x/tools/cover"
)

type config struct {
	Path      string
	IsVerbose bool
}

// map[package]map[file]map[line]covered
type coverageByFile map[string]map[string]map[int]bool

type coverageRow struct {
	Package  string
	Coverage float32
}

type report []coverageRow

func main() {
	var c = config{}
	fs := flag.CommandLine
	fs.StringVar(&c.Path, "f", "coverage.txt", "The name of the coverage file to parse")
	fs.BoolVar(&c.IsVerbose, "v", false, "Be verbose")
	if err := fs.Parse(os.Args[1:]); err != nil {
		log.Fatalf("parsing command line: %v", err)
	}
	profs, err := cover.ParseProfiles(c.Path)
	if err != nil {
		log.Fatalf("loading coverage file %s: %v", c.Path, err)
	}
	if c.IsVerbose {
		log.Printf("coverage loaded: %d profiles", len(profs))
	}

	byFile := calcCoverageByFile(profs)
	byPackage := coverageByPackage(byFile)
	tw := tabwriter.NewWriter(os.Stdout, 0, 0, 1, ' ', 0)
	aggregate := float32(0.0)
	for _, coverage := range byPackage {
		if c.IsVerbose {
			fmt.Fprintf(tw, "%s\t%6.3f\n", coverage.Package, coverage.Coverage)
		}
		aggregate += coverage.Coverage
	}
	ratio := aggregate / float32(len(byPackage))
	if c.IsVerbose {
		fmt.Fprintf(tw, "%s\t%6.3f\n", "Global, unweighted", ratio)
	} else {
		fmt.Fprintf(tw, "%.3f\n", ratio)
	}

	tw.Flush()
}

func calcCoverageByFile(profs []*cover.Profile) coverageByFile {
	work := coverageByFile{}
	for _, prof := range profs {
		dir := filepath.Dir(prof.FileName)
		pkg := dir // TODO add naming protection logic for package name
		if work[pkg] == nil {
			work[pkg] = map[string]map[int]bool{}
		}
		if work[pkg][prof.FileName] == nil {
			work[pkg][prof.FileName] = map[int]bool{}
		}

		for _, block := range prof.Blocks {
			// Coverage merges.
			if block.Count > 0 {
				for line := block.StartLine; line <= block.EndLine; line++ {
					work[pkg][prof.FileName][line] = true
				}
				continue
			}
			// Non-coverage does not merge: only add not-yet counted lines.
			for line := block.StartLine; line <= block.EndLine; line++ {
				if _, ok := work[pkg][prof.FileName][line]; ok {
					continue
				}
				work[pkg][prof.FileName][line] = false
			}
		}
	}
	return work
}

func coverageByPackage(bf coverageByFile) report {
	res := make(report, 0, len(bf))
	var y, n float32
	for pkg, files := range bf {
		for _, file := range files {
			for _, set := range file {
				if set {
					y++
				} else {
					n++
				}
			}
		}
		if y+n == 0 {
			log.Fatalf("Unexpected 0 row count on package %s", pkg)
		}
		coverage := y / (y + n)
		res = append(res, coverageRow{
			Package:  pkg,
			Coverage: coverage,
		})
	}
	sort.Slice(res, func(i, j int) bool {
		return strings.Compare(res[i].Package, res[j].Package) < 0
	})
	return res
}
